#!/bin/bash
set -euxo pipefail

TOPDIR=$1

# Print some information about the machine
df -h
free -h

# osbuild-composer-tests contains repo overrides for testing. Override the
# overrides with latest compose. (We don't want to rely on latest compose
# upstream as it randomly breaks the CI.)
cp $TOPDIR/repositories/* /usr/share/tests/osbuild-composer/repositories/

# The tests put logs in WORKSPACE, create a temporary dir for this purpose.
# See https://github.com/osbuild/osbuild-composer/issues/1096
mkdir /tmp/osbuild-composer-tests
export WORKSPACE=/tmp/osbuild-composer-tests

# In a perfect world, we should be able to run all test executables just by
# iterating over them. Sadly, this is not possible, see the comments.
/usr/libexec/tests/osbuild-composer/base_tests.sh
/usr/libexec/tests/osbuild-composer/filesystem.sh
/usr/libexec/tests/osbuild-composer/regression.sh

# /usr/libexec/tests/osbuild-composer/image_tests.sh requires nested virt
# not available in OSCI and it also doesn't support turning off the booting
# test. Work around it by calling the underlying executable directly as
# it supports the -disable-local-boot argument.
/usr/libexec/osbuild-composer-test/osbuild-image-tests \
    -disable-local-boot -fail-local-boot=false \
    -skip-selinux-ctx-check \
    /usr/share/tests/osbuild-composer/manifests/rhel_90-*

# tests not invoked:
# - api.sh                          - requires secrets, we cannot pass them safely in OSCI.
# - aws.sh                          - requires secrets, we cannot pass them safely in OSCI.
# - azure*.sh                       - requires secrets, we cannot pass them safely in OSCI.
# - installers.sh                   - requires nested virtualization, not available in OSCI.
# - koji.sh                         - not supported on RHEL.
# - libvirt.sh                      - requires nested virtualization (8.5+), not available in OSCI.
#   (see https://bugzilla.redhat.com/show_bug.cgi?id=1975242 )
# - ostree.sh                       - requires nested virtualization, not available in OSCI.
# - ostree-ng.sh                    - requires nested virtualization, not available in OSCI.
# - ostree-raw-image.sh             - requires nested virtualization, not available in OSCI.
# - ostree-simplified-installer.sh  - requires nested virtualization, not available in OSCI.
# - vmware.sh                       - requires secrets, we cannot pass them safely in OSCI.
